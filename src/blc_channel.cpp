/*
 Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2015)
 
 Author:  Arnaud Blanchard
 
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
  users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
  In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
  and, more generally, to use and operate it in the same conditions as regards security.
  The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. *
 
 
 Created on: Oct 9, 2014
 Author: Arnaud Blanchard
 */

#include "blc_channel.h"

#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <pthread.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/mman.h> //mmap
#include <sys/stat.h> // mode S_ ... constants
#include <sys/time.h> //gettimeofday
#include <semaphore.h>
#include "blc_core.h"

#define TMP_BUFFER_SIZE 4096
#define BLC_CHANNELS_LIST_PATH "/tmp/blc_channels.txt"

static int blc_channel_id_max = 0;

blc_channel::blc_channel(): id(-1), access_mode(0), fd(-1), sem_new_data(NULL), sem_ack_data(NULL){}

blc_channel::blc_channel(char const *new_name, int mode){
    open(new_name, mode);
}

blc_channel::blc_channel(char const *new_name, int mode, uint32_t type, uint32_t format, int dims_nb, int length, ...):access_mode(0), fd(-1), sem_new_data(NULL), sem_ack_data(NULL)
{
    va_list arguments;
    
    va_start(arguments, length);
    vcreate_or_open(new_name, mode, type,format, dims_nb, length, arguments);
    va_end(arguments);
}

//Should use code in common with fprint_info
void blc_channel::sprint_info(char *buffer, size_t buffer_size){
    int width, local_width;
    uint32_t str_type;
    uint32_t str_format;
    
    if (type==0) EXIT_ON_ERROR("The type should not be null for '%s'. Use 'NDEF' by default.", name);
    if (format==0) EXIT_ON_ERROR("The format should not be null for '%s'. Use 'NDEF' by default.", name);
    if (parameter[0]==0) EXIT_ON_ERROR("The parameter should not be null for '%s'. Use \"NDEF\" by default.", name);
    
    width=snprintf(buffer, buffer_size, "%.4s %.4s %-31s ", UINT32_TO_STRING(str_type, type),  UINT32_TO_STRING(str_format, format), parameter);
    local_width=sprint_dims(buffer+width,buffer_size-width);
    width+=local_width;
    width+=snprintf(buffer+width, buffer_size-width,  "%*c", 32-local_width, ' ');
    if (strlen(name)==0) EXIT_ON_ERROR("name is empty");
    snprintf(buffer+width, buffer_size-width," %s\n", name);
}

void blc_channel::fprint_info(FILE *file, int print_id){
    int width;
    uint32_t net_type = htonl(type);
    uint32_t net_format = htonl(format);
    
    if (type==0) EXIT_ON_ERROR("The type should not be null for '%s'. Use NDEF by default.", name);
    if (format==0) EXIT_ON_ERROR("The format should not be null for '%s'. Use NDEF by default.", name);
    if (parameter[0]==0) EXIT_ON_ERROR("The parameter should not be null for '%s'. Use NDEF by default.", name);
    
    if (print_id) {
        SYSTEM_ERROR_CHECK(fprintf(file, "%.6d ", id),-1,"print id");
    }
    
    fprintf(file, "%.4s %.4s %-31s ", (char*)&net_type, (char*)&net_format, parameter);
    width=fprint_dims(file);
    fprintf(file, "%*c", 32-width, ' ');
    if (strlen(name)==0) EXIT_ON_ERROR("name is empty");
    fprintf(file, " %s\n", name);
}

//Should use code in common with fscan_info
int blc_channel::sscan_info(char const *string){
    int ret, pos;
    
    dims_nb=0;
    data=NULL;
    size=0;
    
    ret = sscanf(string, "%4c %4c %31s%n", (char*)&type, (char*)&format, parameter, &pos);
    if (ret == EOF) return -1;
    else if (ret!=3) EXIT_ON_ERROR("Only %d parameters have been read instead of 3 in '%s'.", ret,  string);
    
    string+=pos;
    
    NTOHL(type);
    NTOHL(format);
    
    string+=sscan_dims(string);
    
    if (sscanf(string, " %"STRINGIFY_CONTENT(NAME_MAX)"[^\n]\n", name) != 1) EXIT_ON_ERROR("Impossible to read channel's name of channel id '%d'.", id);
    if (id > blc_channel_id_max) blc_channel_id_max=id;
    return id;
}


//Be this remove replace '.' by '/' in the begining of the name
int blc_channel::fscan_info(FILE *file, int scan_id){
    int ret;
    dims_nb=0;
    data=NULL;
    size=0;
    
    if (scan_id){
        SYSTEM_ERROR_CHECK(ret = fscanf(file, "%d ", &id),-1, "Reading channel id");
        if (ret!=1) EXIT_ON_ERROR("impossible to read id");
    }
    ret = fscanf(file, "%4c %4c %31s", (char*)&type, (char*)&format, parameter);
    if (ret == EOF) return -1;
    else if (ret!=3) EXIT_ON_ERROR("%d parameters have been read instead of 3 in " BLC_CHANNELS_LIST_PATH" id '%d'.", ret, id);
    
    NTOHL(type);
    NTOHL(format);
    
    fscan_dims(file);
    
    if (fscanf(file, " %"STRINGIFY_CONTENT(NAME_MAX)"[^\n]\n", name) != 1) EXIT_ON_ERROR("Impossible to read channel's name of channel id '%d'.", id);
    
    if (id > blc_channel_id_max) blc_channel_id_max=id;
    return id;
}

void blc_channel::map_memory(int access_mode){
    int prot=0;
    
    this->access_mode=access_mode;
    
    switch (access_mode & O_ACCMODE){
        case O_RDONLY:prot=PROT_READ;break;
        case O_RDWR:prot=PROT_WRITE | PROT_READ; break;
        default: EXIT_ON_CHANNEL_ERROR(this, "Unknown mode '%d'.", access_mode);
    }
    
    SYSTEM_ERROR_CHECK(data = mmap(0, size, prot, MAP_SHARED, fd, 0), MAP_FAILED,  "Mapping memory of %s (size %ld bytes), fd(%d), prot(%d).", name, size, fd, prot);
    if (data==NULL) EXIT_ON_CHANNEL_ERROR(this, "Fail mapping blc_channel");
    blc_channel_post_event();
}

void blc_channel::open_semaphores(int create){
    char tmp_name[NAME_MAX];
    
    sprintf(tmp_name, "blc_channel%d_sem_new_data0", id);
    
    if (create) SYSTEM_ERROR_CHECK(sem_new_data = sem_open(tmp_name, O_CREAT | O_EXCL, S_IRWXU, 0), SEM_FAILED, "Creating named semaphore '%s' for blc_channel '%s'", tmp_name, name);
    else SYSTEM_ERROR_CHECK(sem_new_data = sem_open(tmp_name, NO_FLAG), SEM_FAILED, "Opening named semaphore '%s' for blc_channel '%s'", tmp_name, name);
    
    sprintf(tmp_name, "blc_channel%d_sem_ack_data0", id);
    if (create) SYSTEM_ERROR_CHECK(sem_ack_data = sem_open(tmp_name, O_CREAT | O_EXCL, S_IRWXU, 1), SEM_FAILED, "Creating named semaphore '%s' for blc_channel '%s'", tmp_name, name);
    else SYSTEM_ERROR_CHECK(sem_ack_data = sem_open(tmp_name, NO_FLAG), SEM_FAILED, "Opening named semaphore '%s' for blc_channel '%s'", tmp_name, name);
}

///Create a channel once the blc_array is already defined
void blc_channel::create(char const *new_name, int access_mode){
    blc_channel info;
    FILE *file;
    int sync_new_data=0, sync_ack_data=0;
    
    strcpy(parameter, "NDEF");
    STRCPY(name, new_name);
    
    switch (name[0]){
        case '/':
        break;
        case '.':
        sync_new_data=1;
        break;
        case '^':
        sync_ack_data=1;
        break;
        case ':':
        sync_new_data=1;
        sync_ack_data=1;
        break;
        default:EXIT_ON_ERROR("Blc channel names must start with '/' for asynchrone mode or with '.',';',':' for synchrone mode. But it is '%s'", name);
        break;
    }
    name[0]='/'; //In any case the name start with '/'
    
    id = blc_channel_get_info_with_name(&info, name);
    if (id !=-1 ) EXIT_ON_ERROR("The channel '%s' is already referenced in '" BLC_CHANNELS_LIST_PATH"'.\nYou may unlink it before:\n    blc_channels --unlink %s", info.name);
    if (blc_channel_id_max == BLC_CHANNELS_MAX) EXIT_ON_CHANNEL_ERROR(this, "creation impossible, channel id (%d) too big", blc_channel_id_max);
    else blc_channel_id_max++;
    id = blc_channel_id_max;
    
    SYSTEM_ERROR_CHECK(file=fopen(BLC_CHANNELS_LIST_PATH, "a+"), NULL, "Openning the file '" BLC_CHANNELS_LIST_PATH"' in order to reference the channel '%s'.", name);
    
    fd = shm_open(name, O_CREAT | O_EXCL | O_RDWR, S_IRWXU); // (O_EXCL first) is important in order to avoid a race condition.  We create it at the same time we check it does not exist. Otherwise someone else could create it in between.
    
    if (fd==-1){//Creation impossible
        fclose(file);
        if (errno == EEXIST) EXIT_ON_ERROR("The shared memory '%s' already exists, you should destroy it before. e.g.:\n    blc_channels --unlink%s", name, name);
        else EXIT_ON_SYSTEM_ERROR("Creating shared memory '%s'.", name);
    }
    else{
        fprint_info(file, 1);
        fclose(file);
        
        open_semaphores(1);
        
        if (sync_new_data==0){
            sem_close(sem_new_data);
            sem_new_data=NULL;
        }
        
        if (sync_ack_data==0){
            sem_close(sem_ack_data);
            sem_ack_data=NULL;
        }
        
        SYSTEM_ERROR_CHECK(ftruncate(fd, size), -1, "fd:%d, size '%ld' for channel '%s'.", fd, size, name);
        if (access_mode==BLC_CHANNEL_READ){
            close(fd);
            fd = shm_open(name, O_RDONLY, S_IRWXU);
        }
        map_memory(access_mode);
    }
}

///Create a blc channel
void blc_channel::create(char const *new_name, int access_mode, uint32_t type, uint32_t format, int dims_nb, blc_dim *dims){
    def_array(type, format, dims_nb, dims);
    create(new_name, access_mode);
}

///Create a blc channel
void blc_channel::vcreate(char const *new_name, int mode, uint32_t type, uint32_t format, int dims_nb, size_t length, va_list arguments){
    vdef_array(type, format, dims_nb, length, arguments);
    create(new_name,  mode);
}

void blc_channel::create(char const *new_name, int mode, uint32_t type, uint32_t format, const char *size_string){
    if (size_string==NULL) EXIT_ON_SYSTEM_ERROR("You cannot have null dimension");
    def_array(type, format, size_string);
    create(new_name, mode);
}

void blc_channel::create(char const *new_name, int mode,  uint32_t type, uint32_t format, int dims_nb, size_t length0, ...){
    va_list arguments;
    
    va_start(arguments, length0);
    vcreate(new_name, mode,  type, format, dims_nb, length0, arguments);
    va_end(arguments);
}

int blc_channel::conflict(const char *new_name){
    int id, dim, conflict_id=0;
    blc_channel info;
    
    id = blc_channel_get_info_with_name(&info, new_name);
    if (id!=-1) {
        if (info.dims_nb!=dims_nb) conflict_id=1;
        else if (info.type!=type) conflict_id=2;
        else if (info.format!=format) conflict_id=3;
        else if (info.dims_nb!=0){
            for(dim=0; dim != info.dims_nb; dim++) {
                if (dims[dim].length!=info.dims[dim].length) {
                    conflict_id=4;
                    break;
                }
            }
        }
    }
    return conflict_id;
}

///Open an existing channel
void blc_channel::open(const char *name, int access_mode){
    char name_tmp[NAME_MAX];
    int sync_new_data=0, sync_ack_data=0, ret;
    
    
    STRCPY(this->name, name);
    
    switch (name[0]){
        case '/':
        break;
        case '.':
        sync_new_data=1;
        break;
        case '^':
        sync_ack_data=1;
        break;
        case ':':
        sync_new_data=1;
        sync_ack_data=1;
        break;
        default:EXIT_ON_ERROR("Blc channel names must start with '/' for asynchrone mode or with '.','^',':' for synchrone mode. But it is '%s'", this->name);
        break;
    }
    this->name[0]='/'; //In any case the nale start with '/'
    id = blc_channel_get_info_with_name(this, name);
    if (id==-1) EXIT_ON_ERROR("The blc_channel '%s' does not exist. Run  blc_channels", name);
    
    SYSTEM_ERROR_CHECK(fd = shm_open(this->name, access_mode, S_IRWXU), -1, "Impossible to open shared memory '%s'with mode %d.", name, access_mode);
    
    
    
    if (sync_new_data) {
        sprintf(name_tmp, "blc_channel%d_sem_new_data0", id);
        SYSTEM_ERROR_CHECK(sem_new_data = sem_open(name_tmp, NO_FLAG), SEM_FAILED, "Opening named semaphore '%s' for blc_channel '%s'", name_tmp, name);
        /*
         if (access_mode==BLC_CHANNEL_READ){ //As we have just started. Any data is new
         while ((ret=sem_trywait(sem_new_data))==0);
         if (errno!=EAGAIN) EXIT_ON_SYSTEM_ERROR("blc_channel '%s'", this->name);
         { //If the semaphore is busy we post it until it is unlocked
         
         
         if (errno==EAGAIN) SYSTEM_ERROR_CHECK(sem_post(sem_new_data), -1,  "blc_channel '%s'", this->name);
         else EXIT_ON_SYSTEM_ERROR("blc_channel '%s'", this->name);
         }
         //   SYSTEM_ERROR_CHECK(sem_post(sem_new_data), -1,  "blc_channel '%s'", this->name);
         }*/
    }
    
    if (sync_ack_data){
        sprintf(name_tmp, "blc_channel%d_sem_ack_data0", id);
        SYSTEM_ERROR_CHECK(sem_ack_data = sem_open(name_tmp, NO_FLAG), SEM_FAILED, "Opening named semaphore '%s' for blc_channel '%s'", name_tmp, name);
        
     /*   if (access_mode==BLC_CHANNEL_READ){
            if (sem_new_data){
                if (sem_trywait(sem_new_data)==0){
                    while(sem_trywait(sem_new_data)==0);
                    if (errno!=EAGAIN) EXIT_ON_SYSTEM_ERROR("blc_channel '%s' sem_new_data", this->name);
                    sem_post(sem_new_data); //new 1
                    fprintf(stderr, "%s: new 1\n", blc_program_name);
                    
                }
                else{ //new 0
                    fprintf(stderr, "new 0\n");
                    
                    if (errno==EAGAIN){
                        while(sem_trywait(sem_ack_data)==0);
                        sem_post(sem_ack_data); // We set ack 1
                    }
                    else EXIT_ON_SYSTEM_ERROR("blc_channel '%s'", this->name);
                }
            }
        }
        else if (access_mode==BLC_CHANNEL_WRITE){
            if (sem_new_data){
                if (sem_trywait(sem_ack_data)==0) { //ack 1
                    fprintf(stderr, "ack 1\n");
                    while(sem_trywait(sem_ack_data)==0);
                    if (errno!=EAGAIN) EXIT_ON_SYSTEM_ERROR("blc_channel '%s' sem_ack_data", this->name);
                    sem_post(sem_ack_data);
                    while(sem_trywait(sem_new_data)==0); //We set new 0
                    if (errno!=EAGAIN) EXIT_ON_SYSTEM_ERROR("blc_channel '%s' sem_new_data", this->name);
                }
                else {
                    fprintf(stderr, "ack 0\n");
                    
                    if (errno!=EAGAIN) EXIT_ON_SYSTEM_ERROR("blc_channel '%s' sem_ack_data", this->name);
                }
            }
        }*/
    }
    map_memory(access_mode);
}


int blc_channel::create_or_open(char const *new_name, int access_mode, uint32_t type, uint32_t format, int dims_nb, blc_dim *dims){
    int id, dim, created;
    uint32_t new_type_str, new_format_str;
    blc_channel info;
    
    id = blc_channel_get_info_with_name(&info, new_name);
    if (id==-1) {
        create(new_name, access_mode, type, format, dims_nb, dims);
        created=1;
    }
    else{
        if (info.dims_nb!=dims_nb) EXIT_ON_CHANNEL_ERROR(&info, "Reopening a blc_channel with different dims_nb '%d'. You may want to unlink it:\n    blc_channels  --unlink %s", dims_nb, info.name);
        if (info.type!=type) EXIT_ON_CHANNEL_ERROR(&info, "Reopening blc_channel with another type '%.4s'. You may want to unlink it:\n    blc_channels --unlink %s", UINT32_TO_STRING(new_type_str, type), info.name);
        if (info.format!=format) EXIT_ON_CHANNEL_ERROR(&info, "Reopening blc_channel with another format '%.4s'. You may want to unlink it:\n    blc_channels --unlink %s", UINT32_TO_STRING(new_format_str, format), info.name);
        if (info.dims_nb!=0){
            for(dim=0; dim != info.dims_nb; dim++) {
                if (dims[dim].length!=info.dims[dim].length)  EXIT_ON_ERROR("The reopening dimension '%d' length '%d' of '%s' differ from the existing one '%d'. You may want to unlink the blc_channel before:\n    blc_channels --unlink %s", dim, dims[dim].length,  info.name, info.dims[dim].length, info.name);
            }
        }
        open(new_name, access_mode);
        created=0;
    }
    return created;
}

///Create a blc channel
int blc_channel::vcreate_or_open(char const *new_name, int access_mode, uint32_t type, uint32_t format, int dims_nb, size_t length0, va_list arguments){
    blc_dim *dims;
    size_t size;
    
    dims=vcreate_blc_dims(&size, type, dims_nb, length0, arguments);
    return create_or_open(new_name, access_mode, type, format, dims_nb, dims);
}

int blc_channel::create_or_open(char const *new_name, int mode, uint32_t type, uint32_t format, int dims_nb, size_t length0, ...)
{
    va_list arguments;
    int created;
    
    va_start(arguments, length0);
    created=vcreate_or_open(new_name, mode,  type, format,  dims_nb, length0, arguments);
    va_end(arguments);
    return created;
}

int blc_channel::create_or_open(char const *new_name, int mode, uint32_t type, uint32_t format, char const *dims_string){
    int  created=1;
    size_t length;
    int pos, local_dims_nb;
    uint32_t new_type_str, new_format_str;
    
    id = blc_channel_get_info_with_name(this, new_name);
    if (id==-1) create(new_name, mode, type, format, dims_string);
    else{
        if (this->dims_nb != dims_nb) EXIT_ON_CHANNEL_ERROR(this, "Reopening a blc_channel with different dims_nb '%d'.", dims_nb);
        if (this->type!=type) EXIT_ON_CHANNEL_ERROR(this, "Reopening blc_channel with another type '%.4s'", UINT32_TO_STRING(new_type_str, type));
        if (this->format!=format) EXIT_ON_CHANNEL_ERROR(this, "Reopening blc_channel with another format '%.4s'", UINT32_TO_STRING(new_format_str, format));
        
        SSCANF(1, dims_string, "%lu%n", &length, &pos);
        
        local_dims_nb=0;
        do{
            dims_string+=pos;
            if (dims[local_dims_nb].length != length) EXIT_ON_CHANNEL_ERROR(this, "Reopening blc_channel with a different length '%lu' on dim '%d'", length, local_dims_nb);
            local_dims_nb++;
            if (local_dims_nb > dims_nb)  EXIT_ON_CHANNEL_ERROR(this, "Reopening blc_channel with a bigger dims_nb '%d'", local_dims_nb);
        }while(sscanf(dims_string, "x%lu%n", &length, &pos)==1);
        
        if (local_dims_nb<dims_nb) EXIT_ON_CHANNEL_ERROR(this, "Reopening blc_channel with a smaller dims_nb '%d'", local_dims_nb);
        open(new_name, mode);
        created=0;
    }
    return created;
}

int blc_channel::create_or_open(char const *name, int mode){
    return create_or_open(name, mode, type, format, dims_nb, dims);
}

blc_channel::~blc_channel(){
    
    if (fd!=-1){ //if fd is -1 i.e only the blc_array part af this class has been used
        
        if (data){
            SYSTEM_SUCCESS_CHECK(munmap(data, size), 0, "Closing blc_channel '%s'", name);
            data=NULL;
        }
        
        close(fd);
        blc_channel_post_event();
    }
}

void blc_channel::remove(){
    blc_remove_channel_with_name(name);
	if (dims) FREE(dims);
	if (data){
	            SYSTEM_SUCCESS_CHECK(munmap(data, size), 0, "Closing blc_channel '%s'", name);
	            data=NULL;
	        }

	        close(fd);
	        blc_channel_post_event();

}

void blc_channel::publish(){
    char sync_char;
    
    if (sem_new_data && sem_ack_data) sync_char=':'; // <=>
    else if (sem_new_data)  sync_char='.'; // ->
    else if (sem_ack_data)  sync_char='\''; // <-
    else sync_char='/';   //No synchronisation
    
    fprintf(stdout, "%c%s\n", sync_char, name+1); //Replace first '/' by '.'
    
    //This is needed with pipe. "\n" dos not flush whane the program is piped
    fflush(stdout);
}

void blc_channel::fprint_debug(FILE *file){
    uint32_t type_str, format_str;
    
    fprintf(file, "\nblc_channel:\n name:%s, type:%.4s, format:%.4s, id:%d, fd:%d, mode:%d, dims_nb:%d, size:%ld \n", name, UINT32_TO_STRING(type_str, type), UINT32_TO_STRING(format_str, format), id, fd, access_mode, dims_nb, size);
    fprint_dims(file);
    fprintf(file, "\n");
    if (data==NULL) fprintf(file, "data is null\n");
}


int blc_channel::post_new_data(){
    
    int listenner = -1;
    if (this->sem_new_data) {
        if (sem_trywait(this->sem_new_data)==0) listenner=0;
        else if (errno==EAGAIN) listenner=1;
        else EXIT_ON_SYSTEM_ERROR("blc_channel '%s'", this->name); // EAGAIN just means semaphore is busy
        
        SYSTEM_ERROR_CHECK(sem_post(this->sem_new_data), -1, "Sem new_data of blc_channel '%s'", this->name); //We signal new_data
    }
    return listenner;
}

int blc_channel::post_ack_data(){
    
    int listenner = -1;
    if (this->sem_new_data) {
        if (sem_trywait(this->sem_ack_data)==0) listenner=0;
        else if (errno==EAGAIN) listenner=1;
        else EXIT_ON_SYSTEM_ERROR("blc_channel '%s'", this->name); // EAGAIN just means semaphore is busy
        
        SYSTEM_ERROR_CHECK(sem_post(this->sem_ack_data), -1, "Sem new_data of blc_channel '%s'", this->name); //We signal new_data
    };
    
    return listenner;
}


