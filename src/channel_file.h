#ifndef CHANNEL_FILE_H
#define CHANNEL_FILE_H

#include "blc_channel.h"

START_EXTERN_C
/**Used by EXIT_ON_CHANNEL_ERROR*/
void blc_channel_fatal_error(blc_channel *channel, const char *name_of_file, const char* name_of_function, int numero_of_line, const char *message, ...);
/**Create a blc_channel with the name, the mode is BLC_CHANNEL_READ or BLC_CHANNEL_WRITE, the type of data UIN8, INT8, UI16, IN16, FL32, FL64. ( default 0:UIN8),
 The format are four chars user defined but usually describe the image format  'Y800' (for grey), 'RGB3', 'YUV2' ,...,.
 Dims_nb is the number of dimenssions of the data ( 1 for a vector).
 The following values are the the length of each dims. You must have the same number of length than dims_nb*/
void blc_channel_create(blc_channel* channel, const char *name, int mode,  uint32_t type, uint32_t format, int dims_nb, int length0, ...);
/**Open an exisiting channel 'name' */
void blc_channel_open(blc_channel* channel, const char *name, int mode);
/**Open an exisiting channel 'name' or create it if it does not exist */
int blc_channel_create_or_open(blc_channel *channel, const char *name, int mode,  uint32_t type, uint32_t format, int dims_nb, int length0, ...);
/** Close the channel and unlink the shared memory. No new process can use it.*/
int blc_channel_remove(blc_channel *channel);
/** Allocate (need to be free) and give the array of existing shared memory files on the system.*/
int blc_channel_get_all_infos(blc_channel **channels_infos);
/** Allocate (need to be free) and give the array of existing shared memory files on the system.*/
int blc_channel_get_all_infos_with_filter(blc_channel **channels_infos, char const *start_name_filter);
///Retrieve the informations about a channel if it is known and return the id. Otherwise, return -1. You need to free .dims, which is allocated to contain the list of blc_dims.
int blc_channel_get_info_with_name(blc_channel *info, char const *name);
///Remove the blc_channel **name**. The other processes using it will still work but no new ones can use it.
void blc_remove_channel_with_name(char const *name);
END_EXTERN_C

#endif
