BLC channel
===========

- Copyright : [ETIS](http://www.etis.ensea.fr/neurocyber) - ENSEA, University of Cergy-Pontoise, CNRS (2011-2016)  
- Author    : [Arnaud Blanchard](http://arnaudblanchard.thoughtsheet.com)  
- Licence   : [CeCILL v2.1](http://www.cecill.info/licences/Licence_CeCILL_V2-en.html)  

Functions to easily share data through shared memory (shm_... functions). It is the fastest, and the more econom in memory ( no copy in each process ), way to share informtation between processes. At this moment it only works in asynchronous mode.

The idea is that you have a structure blc_channel which is like a blc_array ( https://framagit.org/blaar/blc_core/blob/master/t_array/main.cpp ) but has a name starting with '/' to identificate it on your system.

Example
=======

For exemple you create a channel in one program to write data:

```c++
//Creating shared called '/my_channel_name', this program will write on it, it is char ('INT8') with no specific format ('NDEF') of dimension 1 (vector) of 32 elements.
    
blc_channel my_channel.create("/my_channel_name", BLC_CHANNEL_WRITE, 'INT8', 'NDEF', 1, 32);
snprintf(my_channel.chars, 32, "Hello world !\n");
```
    
and read this data in another program:

```c++
//Opening shared memory called '/my_channel_name', this program will only read it.

blc_channel my_receiving_channel.open("/my_channel_name", BLC_CHANNEL_READ);
printf("%s", my_receiving_channel.chars);
```
     
The second program prints 'Hello world !'.

Demo
====

`blc_channel/demo.sh` launches two processes. 
- **t_channel_reader** reading each second the content of '/channel_example' and printing it.
- **t_channel_writer** waiting for the user to enter text and filing the channel '/channel_example' with this text.  Each second the reader will print this text. 

Press 'q' to quit.

Details
=======

The information about the properties of the blc_channels (type, format, sizes) is stored in a special file: '/tmp/blc_channels.txt'. You are not suppose to use it but you can check at anytime the status of the blc_channels using `./run.sh blc_channels` in your blaar directory.

You will see all the existing blc_channels, the process reading or writing and the possibility to destroy channels.

On Linux we can see and manipulate a virtual file containing this memory in /run/shm/<name of your shared memory>, on OSX you cannot but anyway it is only used for debug.

Synchronisation
===============

Two semaphores associated to the shared memory are created:

- `blc_channle<id>_new_data0` which indicates that new data is available on the shared memory
- `blc_channle<id>_ack_data0` which acknowledges that the data has been read

Proccesses which do not consider synchronisation use starting char '/'

`[writer]/->[reader]` means no synchronization
`[writer].->[reader]` means the reader waits for new data from the writer
`[writer]^->[reader]` means the writer waits for the reader to read and acknowledge the data
`[writer]:->[reader]` means both direction synchronization. The writer waits for acknowledgement and the reader wait for new data from the writer.

While opening a channel
-----------------------

In synchronous mode, we assume there is only one reader and writter at a time

- **new 0, ack 1**: The data has been read, the receiver is ready
- **new 1, ack 0**: New data is ready for reading
- **new 0, ack 0**(1): The data is being read or written. This is a scenario where you do not know if you have to wait or if the channel was badly closed
- **new 1, ack 1**(2): The new data has not been read yet. This must not happen as the risk is that you can read and write at the same time

At the initialisation, when a channel is created, **ack** is set to 1


If you have a problem, case (1) you can use the tools blc_channels to manually unlock **ack** or **new**. 

C/C++ documentation
===================

To see the documentation for C++, execute : `./doc_api blc_channel` in your blaar directory. Add `-c` option if you want the plain C api.


