/* Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2015)
 
 Author: Aranud Blanchard
 
 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
  users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
  In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
  and, more generally, to use and operate it in the same conditions as regards security.
  The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. */



#ifndef BLC_CHANNEL_H
#define BLC_CHANNEL_H

#include "blc_core.h"

#include <limits.h> //< NAME_MAX, ...
#include <fcntl.h> //< O_... constants O_RDONLY, 0_RDWR, O_WRONLY
#include <semaphore.h>

/**\mainpage Summary
The principle of blc_channel is to share data among different process through shared memory.
Shared memory are opened with POSIX shm_open. The file /tmp/blc_channels.txt maintain a list of all the blc_channels.
 
 - \ref blc_channel
 */

/**\defgroup blc_channel blc_channel functions
 \{
 */

///Max number of blc_channels
#define BLC_CHANNELS_MAX UINT16_MAX

///Define channel in reading mode only
#define BLC_CHANNEL_READ    O_RDONLY
///Defines channel in writting mode ( if memory can be written, we cannot disable reading )
#define BLC_CHANNEL_WRITE   O_RDWR

///Size of parameters for a channel description. Parameter is not used at this moment
#define BLC_PARAMETER_MAX 31
///Like EXIT_ON_ERROR but also display all debug informatin about the blc_channel
#define EXIT_ON_CHANNEL_ERROR(channel, ...) blc_channel_fatal_error(channel, __FILE__, __FUNCTION__, __LINE__, __VA_ARGS__)

///Extends **blc_array** allowing it to be share among processes through shared memory
typedef struct blc_channel
#ifdef __cplusplus
:blc_array {
    
    ///Constructor initiating an empty blc_channel
    blc_channel();
    ///Constructor opening the blc_channel in reading mode by default (otherwise use BLC_CHANNEL_WRITE)
    blc_channel(char const *new_name, int mode);
    ///Constructor creating or opening the channel if it exist. In this case it has to be compatible with its properties.
    blc_channel(char const *name, int mode,  uint32_t type, uint32_t format, int dims_nb, int length0, ...);
    //Unmap the memory and close the file but the other informations are kept. The semaphores are not affected. @TODO See what to do with semaphores
    ~blc_channel();
    
    /**Check if the current description of the blc_channel and the blc_channel **name** are compatible. Useful to know if you need to use another name for a new blc_channel or to remove the previous one.
     
     The return value indicates the conflict:
     - 0 if there is no conflict
     - 1 the number of dimensions differs
     - 2 the type differs
     - 3 the format differs
     - 4 one of the dimension does not have the same length*/
    int conflict(const char *name);
    
    /**Set the blc_channel properties with the string (i.e "UIN8 RGB3 3x800x600 /channel_name")*/
    int sscan_info(char const *string);
    /**Set the blc_channel properties with the content of the file (i.e "UIN8 RGB3 3x800x600 /channel_name").
     **scan_id** is a flag precising if the channel id preceeds the properties*/
    int fscan_info(FILE *file, int scan_id);
    
    /**Write the properties of the blc_channel on the buffer*/
    void sprint_info(char *buffer, size_t buffer_size);
    /**Write the properties of the blc_channel in the file. **print_id** is a flag in order to require to print or not the blc_channel id*/
    void fprint_info(FILE *file, int print_id);
    /**Create a new blc_channel with the list of dims described as va_args*/
    void vcreate(char const *name, int mode, uint32_t type, uint32_t format, int dims_nb, size_t length0, va_list arguments);
    /**Create a new blc_channel with the list of dims described as va_args*/
    int vcreate_or_open(char const *name, int mode, uint32_t type, uint32_t format, int dims_nb, size_t length0, va_list arguments);
    /**Creates a new blc_channel with the properties already defined*/
    void create(char const *new_name, int mode);
    /**Creates a new blc_channel and with the dims as a variable list of arguments */
    void create(char const *new_name, int mode, uint32_t  type, uint32_t format, int dims_nb, size_t length0, ...);
    /**Creates a new blc_channel and with the dims as an array of dims*/
    void create(char const *new_name, int mode, uint32_t type, uint32_t format, int dims_nb, blc_dim *dims);
    /**Creates a new blc_channel and with the dims as a string (i.e. "3x800x600") */
    void create(char const *new_name, int mode, uint32_t type, uint32_t format, const char *size_string);
    /**Open a blc_channel with **name** */
    void open(const char *name, int mode);
    /**Create the blc_channel if it does not exist otherwise, open it. In this later case the properties have to be compatible.
     You can use conflict function to check before if there is incompatibility*/
    int create_or_open(char const *new_name, int mode);
    int create_or_open(char const *new_name, int mode, uint32_t type, uint32_t format, int dims_nb, size_t length0, ...);
    int create_or_open(char const *new_name, int mode, uint32_t type, uint32_t format,  int dims_nb, blc_dim *dims);
    int create_or_open(char const *new_name, int mode, uint32_t type, uint32_t format, char const *size_string);
    
    ///Send the name of the channel on stdout. Useful to send this blc_channel on a pipe. It is a simple printf but with a unavoided fflush (with a pipe \n does not have effect)
    void publish();
    ///Displays all the informations about the blc_channel
    void fprint_debug(FILE *file=stderr);
    ///Remove the blc_channel on the system. However other already running processes using it will still work but no new process will be able to use it.
    void remove();
    ///Map the shared memory with the blc_channel
    void map_memory(int mode);
    
    void open_semaphores(int create=0);

    
    /**Post new data if the semaphore exist (otherwise return -1). Return 1, if the data has been used ( sem_new_data busy  which is normal), 0 otherwise ( which means nobody is istening ).*/
    int post_new_data();
    /**Post acknoledged data  the semaphore exist (otherwise return -1). Return 1, if the data has been used ( sem_new_data busy which is normal ), 0 otherwise ( which means nobody is checking that).*/
    int post_ack_data();

#else
    {
        blc_array array;// Not beautiful but makes it easy to convert C++ heritage of "class"  in C struct inclusion.
#endif
        int id, access_mode;
        char name[NAME_MAX+1];
        char parameter[BLC_PARAMETER_MAX+1];
        int fd ; ///< shared memory file descriptor
        sem_t *sem_new_data, *sem_ack_data;
    }blc_channel;
    
    START_EXTERN_C
    
    /** Wait until new data is available
     We use void * to make it ablle to be use in callback*/
    void blc_channel_wait_new_data(void *channel);
    
    /** Wait until sommeone has read and acknoledged the data.
     We use void * to make it ablle to be use in callback*/
    void blc_channel_wait_ack_data(void *channel);

    
    /**Used by EXIT_ON_CHANNEL_ERROR*/
     void blc_channel_fatal_error(blc_channel *channel, const char *name_of_file, const char* name_of_function, int numero_of_line, const char *message, ...);
    /**Create a blc_channel with the name, the mode is BLC_CHANNEL_READ or BLC_CHANNEL_WRITE, the type of data UIN8, INT8, UI16, IN16, FL32, FL64. ( default 0:UIN8),
     The format are four chars user defined but usually describe the image format  'Y800' (for grey), 'RGB3', 'YUV2' ,...,.
     Dims_nb is the number of dimenssions of the data ( 1 for a vector).
     The following values are the the length of each dims. You must have the same number of length than dims_nb*/
    void blc_channel_create(blc_channel* channel, const char *name, int mode,  uint32_t type, uint32_t format, int dims_nb, int length0, ...);
    /**Open an exisiting channel 'name' */
    void blc_channel_open(blc_channel* channel, const char *name, int mode);
    /**Open an exisiting channel 'name' or create it if it does not exist */
    int blc_channel_create_or_open(blc_channel *channel, const char *name, int mode,  uint32_t type, uint32_t format, int dims_nb, int length0, ...);
    /** Close the channel and unlink the shared memory. No new process can use it.*/
    int blc_channel_remove(blc_channel *channel);
    /** Allocate (need to be free) and give the array of existing shared memory files on the system.*/
    int blc_channel_get_all_infos(blc_channel **channels_infos);
    /** Allocate (need to be free) and give the array of existing shared memory files on the system.*/
    int blc_channel_get_all_infos_with_filter(blc_channel **channels_infos, char const *start_name_filter);
    ///Retrieve the informations about a channel if it is known and return the id. Otherwise, return -1. You need to free .dims, which is allocated to contain the list of blc_dims.
    int blc_channel_get_info_with_name(blc_channel *info, char const *name);
    ///Remove the blc_channel **name**. The other processes using it will still work but no new ones can use it.
    void blc_remove_channel_with_name(char const *name);
    
    void blc_channel_post_event();

    void blc_channel_check_for_event(void (*callback)(void*user_data), void *user_data);

    void blc_channel_destroy(blc_channel *channel);

    END_EXTERN_C
    ///@}
    
#endif
