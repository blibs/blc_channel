/* Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2015)

 Author: Aranud Blanchard

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. */

/**
 *
 *  @date Oct 10, 2014
 *  @author Arnaud Blanchard
 @defgroup blc_channel channels
 @{
 */ 

#ifndef BLC_NET_CHANNEL_H
#define BLC_NET_CHANNEL_H

#include "blc_network.h"
#include "blc_channel.h"

#define BLC_NET_CHANNEL_PORT_NAME "33333"


typedef struct blc_net_channel
#ifdef __cplusplus
:blc_channel {
    
    /// @param refresh_period -2 only init the channel, -1 init and update, 0 permanetly refresh as fast as possible, otherwise refresh at the specified period (in µs).
    void init(char const *channel_name, char const *address, int refresh_period = -2);
    
     ///By default timeout of 100ms. return 0 in case of failure, 1 in case of success
    int try_recv_update(int us_timeout = 100000);
 
    ///By default timeout of 100ms and only one trial befire failure
    void recv_update(int us_timeout = 100000, int trials_nb = 1);
    
    ///When refresh_period=-1 it is like get_update but it is not blocking
  //  void start_send_updates(blc_channel *channel, char const*address, int refresh_period=-1);
  //  void send_update(blc_channel *channel, char const*address);
#else 
{
    blc_channel channel; // Not beautiful but makes it easy to convert C++ heritage of "class" in C struct inclusion.
#endif
   blc_network network;
   uint32_t index;
   int refresh_period;
   struct msghdr msghdr;
}blc_net_channel;

extern int blc_channel_port;
void blc_net_channel_start_server(char const* port_name);

//void blc_net_channel_begin_read(blc_net_channel);
#endif
///@}
