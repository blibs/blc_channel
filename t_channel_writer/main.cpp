#include "blc_channel.h"

#define SIZE 1000000

int main(int, char **){
    blc_channel channel;
    
    fprintf(stderr, "CHANNEL_WRITTER: Example of a program sending any data on a channel. Press 'q' return to quit.\n");
        
    //Create or reopen an asynchrone channel "/channel_example" sending data of type int8/char in format undefined format of one dimension (vector) of SIZE.
    channel.create_or_open("/channel_example", BLC_CHANNEL_WRITE, 'INT8', 'NDEF',  1, SIZE);
    do{
        fprintf(stderr, "CHANNEL_WRITTER: Text to send ?...\n");
        fgets(channel.chars, SIZE, stdin);
        
    } while(strncmp(channel.chars, "q\n", 2)!=0);
    
    return EXIT_SUCCESS;
}
