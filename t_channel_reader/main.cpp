/*  Author: Arnaud Blanchard
   Example to demonstrate how to use channel in non synchrone mode.
 */

#include "blc_channel.h"
#include <unistd.h>

#define SIZE 1000000

int main(int, char **){
    blc_channel channel;
    
    fprintf(stderr, "CHANNEL_READER: Example of a program reading a channel.\n");
    
    //Create or open an async channel "/channel_example" receiving data (uint8/uchar) in undefined format in one dimension (vector) of SIZE.
    channel.create_or_open("/channel_example", BLC_CHANNEL_READ, 'INT8', 'NDEF',  1, SIZE);
    
    while(1){ //Quit if content is 'q' return
        sleep(1);
        fprintf(stderr, "CHANNEL_READER: Received text '%s'\n", channel.chars);
    }

    return EXIT_SUCCESS;
}
