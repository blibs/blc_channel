
find_package(blc_core REQUIRED)

find_path(BLC_CHANNEL_INCLUDE_DIR blc_channel.h PATH_SUFFIXES blc_channel)
find_library(BLC_CHANNEL_LIBRARY blc_channel)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(blc_core DEFAULT_MSG BLC_CHANNEL_INCLUDE_DIR BLC_CHANNEL_LIBRARY)

mark_as_advanced(BLC_CHANNEL_INCLUDE_DIR BLC_CHANNEL_LIBRARY )

set(BL_INCLUDE_DIRS ${BLC_CHANNEL_INCLUDE_DIR} ${BL_INCLUDE_DIRS} )
set(BL_LIBRARIES ${BLC_CHANNEL_LIBRARY} ${BL_LIBRARIES} )
