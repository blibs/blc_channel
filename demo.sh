
#We go in blaar directory
cd `dirname $0`/..

echo "Demo to send data from t_channel_writer to t_channel_reader through blc_channel (shared memory) called /channel_example"

./run.sh blc_channel/t_channel_reader &
reader_pid=$!
./run.sh blc_channel/t_channel_writer
pkill -TERM -P $reader_pid #To simplify, this kill the reader and all its children when the writer stop

