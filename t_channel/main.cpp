#include "blc_channel.h"

/* This is an exemple of shared memory (blc_channel).
 In order to simplify everything is done in one program but it works the same in two separated programs.
 A shared memory is created (/t_channel) and two independant blc_channels interact with it.
 */

int main(int, char **){
    blc_channel channel_program1;
    blc_channel channel_program2; // It is useless two structures on the same channel but it is for the example
    int i;
    
    fprintf(stderr, "Exemple of writing and reading a blc_channel\n");
    //Create or reopen a channel of one dimension (vectors) of 4 floats. The format is undef
    channel_program1.create_or_open("/t_channel", BLC_CHANNEL_WRITE, 'FL32', 'NDEF', 1, 4); //in C -> blc_channel_create_or_open(&channel_program1, "/t_channel", BLC_CHANNEL_WRITE, 'FL32', 'NDEF', 1, 4)
    channel_program1.publish(); //display the name of the channel on stdout

    channel_program1.floats[0]=0.111f; //in C  channel_program1.array.floats[0]=0.111f;
    channel_program1.floats[1]=0.222f;
    channel_program1.floats[2]=0.333f;
    channel_program1.floats[3]=0.444f;
    
    //We open the channel a second time. We will be able to read the same data but error if write (_READ).
    channel_program2.open("/t_channel", BLC_CHANNEL_READ); //in  C blc_channel_open(&channel_program2, "/t_channel", BLC_CHANNEL_READ);
    fprintf(stderr, "Initial values\n");
    for(i=0; i!=channel_program2.dims[0].length; i++)  printf("%f\n", channel_program2.floats[i]);
    
    //We remove the channel.
    blc_remove_channel_with_name("/t_channel");
    
    //It will not appear to be accessible anymore for another program but we can still use it.
    fprintf(stderr, "We change the first value eventhough the channel does not exist anymore (the local memory is still available).\n");
    channel_program1.floats[0]=111.111f;  //in C  channel_program1.array.floats[0]=111.111f;
    printf("%f\n", channel_program2.floats[0]);
    
    fprintf(stderr, "We can read again the value even thought one of the structure on the channel have been closed.\n");
    //However it is still accessible with channel_program2.
    for(i=0; i!=channel_program2.dims[0].length; i++)  printf("%f\n", channel_program2.floats[i]);
    
    //As the channels are static, they will be closed and free while leaving
    
    return EXIT_SUCCESS;
}
